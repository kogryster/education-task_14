package ru.gerasimova.tm.exception.empty;

public class EmptyUserException extends AbstractEmptyException {

    public EmptyUserException() {
        super("Error! User ID is empty...");
    }

}
