package ru.gerasimova.tm.exception.empty;

import ru.gerasimova.tm.exception.AbstractException;

public abstract class AbstractEmptyException extends AbstractException {

    public AbstractEmptyException() {
    }

    public AbstractEmptyException(String message) {
        super(message);
    }

    public AbstractEmptyException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractEmptyException(Throwable cause) {
        super(cause);
    }

    public AbstractEmptyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
