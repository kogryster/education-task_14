package ru.gerasimova.tm.exception.empty;

public class EmptyEmailException extends AbstractEmptyException {

    public EmptyEmailException() {
        super("Error! Email is empty...");
    }

}
