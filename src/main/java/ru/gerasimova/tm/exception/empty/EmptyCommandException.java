package ru.gerasimova.tm.exception.empty;

public class EmptyCommandException extends AbstractEmptyException {

    public EmptyCommandException() {
        super("Error! Enter the command...");
    }

}
