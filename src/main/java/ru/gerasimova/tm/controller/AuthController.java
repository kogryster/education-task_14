package ru.gerasimova.tm.controller;

import ru.gerasimova.tm.api.controller.IAuthController;
import ru.gerasimova.tm.api.service.IAuthService;
import ru.gerasimova.tm.api.service.IUserService;
import ru.gerasimova.tm.entity.User;
import ru.gerasimova.tm.util.TerminalUtil;

public class AuthController implements IAuthController {

    private final IAuthService authService;
    private final IUserService userService;

    public AuthController(IAuthService authService, IUserService userService) {
        this.authService = authService;
        this.userService = userService;
    }

    @Override
    public void login() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        authService.login(login, password);
        System.out.println("[OK]");
    }

    @Override
    public void logout() {
        System.out.println("[LOGOUT]");
        authService.logout();
        System.out.println("[OK]");

    }

    @Override
    public void registry() {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL]");
        final String email = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        authService.registry(login, password, email);
        System.out.println("[OK]");
    }

    @Override
    public void viewUserProfile() {
        System.out.println("[SHOW USER PROFILE]");
        final String tempUserId = authService.getUserId();
        final User tempUser = userService.findById(tempUserId);
        System.out.println("Login: " + tempUser.getLogin() + " E-mail: " + tempUser.getEmail() + " ID: " + tempUser.getId());
        System.out.println("[OK]");
    }

    @Override
    public void updateUserPassword() {
        System.out.println("[UPDATE USER PASSWORD]");
        final String currentUserId = authService.getUserId();
        final User currentUser = userService.findById(currentUserId);
        System.out.println("[ENTER NEW PASSWORD FOR " + currentUser.getLogin() + " ]");
        final String newPassword = TerminalUtil.nextLine();
        final User userNewPassword = userService.updateUserPassword(currentUserId, newPassword);
        System.out.println("[OK]");
    }

}
