package ru.gerasimova.tm.bootstrap;

import ru.gerasimova.tm.api.controller.IAuthController;
import ru.gerasimova.tm.api.controller.ICommandController;
import ru.gerasimova.tm.api.controller.IProjectController;
import ru.gerasimova.tm.api.controller.ITaskController;
import ru.gerasimova.tm.api.repository.ICommandRepository;
import ru.gerasimova.tm.api.repository.IProjectRepository;
import ru.gerasimova.tm.api.repository.ITaskRepository;
import ru.gerasimova.tm.api.repository.IUserRepository;
import ru.gerasimova.tm.api.service.*;
import ru.gerasimova.tm.constant.ArgumentConst;
import ru.gerasimova.tm.constant.TerminalConst;
import ru.gerasimova.tm.controller.AuthController;
import ru.gerasimova.tm.controller.CommandController;
import ru.gerasimova.tm.controller.ProjectController;
import ru.gerasimova.tm.controller.TaskController;
import ru.gerasimova.tm.entity.User;
import ru.gerasimova.tm.enumeration.Role;
import ru.gerasimova.tm.exception.empty.EmptyCommandException;
import ru.gerasimova.tm.exception.system.IncorrectCommandException;
import ru.gerasimova.tm.repository.CommandRepository;
import ru.gerasimova.tm.repository.ProjectRepository;
import ru.gerasimova.tm.repository.TaskRepository;
import ru.gerasimova.tm.repository.UserRepository;
import ru.gerasimova.tm.service.*;
import ru.gerasimova.tm.util.TerminalUtil;


public final class Bootstrap {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, authService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, authService);

    private final IAuthController authController = new AuthController(authService, userService);

    private void initUsersAndData() {
        final User test = userService.create("test", "test", "test@test.ru");
        final User admin = userService.create("admin", "admin", Role.ADMIN);
        taskService.create(test.getId(), "task01", "demo01 by test");
        taskService.create(test.getId(), "task02", "demo02 by test");
        taskService.create(test.getId(), "task03", "demo03 by test");
        projectService.create(test.getId(), "Project01", "demo01 by test");
        projectService.create(test.getId(), "Project02", "demo04 by test");
        projectService.create(test.getId(), "Project02", "demo03 by test");
        taskService.create(admin.getId(), "task04", "demo04 by test");
        taskService.create(admin.getId(), "task05", "demo05 by test");
        taskService.create(admin.getId(), "task06", "demo06 by test");
        projectService.create(admin.getId(), "Project04", "demo04 by test");
        projectService.create(admin.getId(), "Project05", "demo05 by test");
        projectService.create(admin.getId(), "Project06", "demo06 by test");
    }

    public void run(final String[] args) {
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        if (parseArgs(args)) System.exit(0);
        initUsersAndData();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyCommandException();
        switch (arg) {
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.LOGIN:
                authController.login();
                break;
            case TerminalConst.LOGOUT:
                authController.logout();
                break;
            case TerminalConst.REGISTRY:
                authController.registry();
                break;
            case TerminalConst.USER_INFO:
                authController.viewUserProfile();
                break;
            case TerminalConst.USER_PASSWORD_UPDATE:
                authController.updateUserPassword();
                break;
            default:
                throw new IncorrectCommandException(arg);
        }

    }

}
